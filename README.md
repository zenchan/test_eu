# TEST

## Stack

##### client: 
- ReactJs
- NextJs
- Typescript

##### server: 
- NodeJs
- NestJs
- Typescript

## Backend

/backend/server

### DEV
```sh
$> yarn
$> yarn start:dev
```

### PROD
```sh
$> yarn
$> yarn build
$> yarn start
```


### DEV

## Frontend

/frontend/client

### DEV
```sh
$> yarn
$> yarn dev
```

### Prod
```sh
$> yarn
$> yarn build
$> yarn start
```
### Utils

#### Lint
```sh
$> yarn lint
```

#### Test
```sh
$> yarn test
```


#### Create new component with standardized code structure
```sh
$> yarn create:component <ComponentName>
```
