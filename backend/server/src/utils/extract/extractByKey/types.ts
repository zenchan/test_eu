export interface ObjectByProperty {
  [key: string]: any;
}

export type ExtractByKey = (
  array: Array<any>,
  key: any,
  value: any,
) => ObjectByProperty;
