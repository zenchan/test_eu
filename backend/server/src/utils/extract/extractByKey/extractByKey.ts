import { ExtractByKey } from './types';

export const extractByKey: ExtractByKey = (array, key, value) => {
  return array.find((el: any) => {
    return el[key] === value;
  });
};

export default extractByKey;
