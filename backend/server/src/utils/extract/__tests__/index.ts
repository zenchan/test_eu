import { extractByKey } from '../extractByKey';

describe('extractByKey', () => {
  it('should return the first element of the array that has the given key-value pair', () => {
    const array = [
      { id: 1, name: 'Alice' },
      { id: 2, name: 'Bob' },
      { id: 3, name: 'Charlie' },
    ];

    expect(extractByKey(array, 'id', 2)).toEqual({ id: 2, name: 'Bob' });
    expect(extractByKey(array, 'name', 'Charlie')).toEqual({
      id: 3,
      name: 'Charlie',
    });
  });

  it('should return undefined if the array does not contain the key-value pair', () => {
    const array = [
      { id: 1, name: 'Alice' },
      { id: 2, name: 'Bob' },
      { id: 3, name: 'Charlie' },
    ];

    expect(extractByKey(array, 'id', 4)).toBeUndefined();
    expect(extractByKey(array, 'name', 'David')).toBeUndefined();
  });
});
