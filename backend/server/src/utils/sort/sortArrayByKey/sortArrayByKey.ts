import * as T from './sortArrayByKey.types';

export const sortArrayByKey = (array: T.ArrayWithkey[], key: string) => {
  array.sort((a, b) => {
    return a[key] - b[key];
  });
  return array;
};

export default sortArrayByKey;
