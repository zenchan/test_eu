import { sortArrayByKey } from '../';

describe('sortArrayByKey', () => {
  it('should sort an array of objects by the specified key in ascending order', () => {
    const array = [
      { id: 1, name: 'Alice' },
      { id: 2, name: 'Bob' },
      { id: 3, name: 'Charlie' },
    ];
    const sortedArray = sortArrayByKey(array, 'id');
    expect(sortedArray).toEqual([
      { id: 1, name: 'Alice' },
      { id: 2, name: 'Bob' },
      { id: 3, name: 'Charlie' },
    ]);
  });

  it('should sort an array of objects by the specified key in descending order', () => {
    const array = [
      { id: 1, name: 'Alice' },
      { id: 2, name: 'Bob' },
      { id: 3, name: 'Charlie' },
    ];
    const sortedArray = sortArrayByKey(array, 'id').reverse();
    expect(sortedArray).toEqual([
      { id: 3, name: 'Charlie' },
      { id: 2, name: 'Bob' },
      { id: 1, name: 'Alice' },
    ]);
  });
});
