export const PLAYERS_API_URL =
  process.env.PLAYERS_API_URL ||
  'https://eurosportdigital.github.io/eurosport-node-developer-recruitment/headtohead.json';
export const PORT = process.env.PORT || 3000;
