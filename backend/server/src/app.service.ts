import { PLAYERS_API_URL } from './env';
import { extractByKey } from './utils';
import { sortArrayByKey } from './utils';
import { Injectable, NotFoundException } from '@nestjs/common';
import axios from 'axios';
import * as T from './app.service.types';

@Injectable()
export class AppService {
  ping(): T.Ping {
    return { ping: 'pong' };
  }
  async players(): Promise<any> {
    const response: any = await axios.get(PLAYERS_API_URL);
    return sortArrayByKey(response.data.players, 'id');
  }
  async player(id: string): Promise<any> {
    const response: any = await axios.get(PLAYERS_API_URL);
    const extractedPlayer = extractByKey(
      response.data.players,
      'id',
      Number(id),
    );
    if (!extractedPlayer) {
      throw new NotFoundException('Player not found');
    }
    return extractedPlayer;
  }
}
