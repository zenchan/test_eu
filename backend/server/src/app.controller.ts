import { Ping as TPing, Players as TPlayers } from './app.service.types';
import { Controller, Get, Param } from '@nestjs/common';
import { AppService } from './app.service';

@Controller('')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @Get('ping')
  ping(): TPing {
    return this.appService.ping();
  }
  @Get('players')
  players(): Promise<TPlayers[]> {
    return this.appService.players();
  }
  @Get('players/:id')
  player(@Param('id') id: any): Promise<TPlayers> {
    return this.appService.player(id);
  }
}
