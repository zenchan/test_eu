mkdir src/Components/$1 && \
mkdir src/Components/$1/__tests__ && \
echo "" > src/Components/$1/__tests__/$1.tsx && \
echo "export * from './$1';
export * as T$1 from './$1.types';
export * as SC$1 from './$1.style';
" > src/Components/$1/index.ts && \
echo "import * as SC from './$1.style';
import * as T from './$1.types';

export const $1: T.$1 = () => (
  <SC.Wrapper>
    $1
  </SC.Wrapper>
); 

export default $1;" > src/Components/$1/$1.tsx && \
echo "import styled from 'styled-components'

export const Wrapper = styled.div(({ theme }) => \`
\`);" > src/Components/$1/$1.style.tsx && \
echo "import { FC } from 'react';

export interface $1Props {
};

export type $1 = FC<$1Props>;" > src/Components/$1/$1.types.ts