export const theme = {
  background: {
    color: 'var(--background-color)',
    elem: {
      primary: 'var(--background-elem-primary)',
      secondary: 'var(--background-elem-secondary)',
    },
  },
  color: {
    primary: 'var(--color-primary)',
    secondary: 'var(--color-secondary)',
    light: 'var(--color-light)',
  },
  fontSize: {
    s: 'var(--font-size-s)',
    m: 'var(--font-size-m)',
    l: 'var(--font-size-l)',
    xl: 'var(--font-size-xl)',
  },
  padding: {
    default: 'var(--padding-default)',
  },
  boxShadow: {
    default: 'var(--box-shadow)',
    hover: 'var(--box-shadow-hover)',
  },
};

export default theme;
