import api from '@/api';
import { playerToSheet } from '@/common/playerToSheet';
import { Bubble } from '@/Components/Bubble';
import { Female, Male, Trophy } from '@/Components/Icons';
import { InfoTable } from '@/Components/InfoTable';
import { BubbleKeyDiv } from '@/Container/PlayerSheets/style';
import { Player, PlayerInfo } from '@/Container/PlayerSheets/types';
import { useRouter } from 'next/router';
import { useEffect, useMemo, useState } from 'react';
import * as SC from './style';

export const PlayerSheet = () => {
  const [player, setPlayer] = useState<Player | null>(null);
  const router = useRouter();

  const fetchPlayer = async () => {
    if (!router?.query?.id) return;
    const response = await api.player(`${router.query.id}`);
    setPlayer(response?.data || null);
  };

  useEffect(() => {
    fetchPlayer();
  }, [router]);

  const playerInfos: PlayerInfo | null = useMemo(() => {
    if (!player) return null;
    return playerToSheet(player);
  }, [player, setPlayer]);

  if (!playerInfos) return <div>Loading...</div>;

  return (
    <SC.Wrapper>
      <SC.ProfileImg src={playerInfos.head.picture} />
      <SC.HeadDiv>
        <SC.FlagImg alt="flag" src={playerInfos.head.flag} />
        <SC.Title>
          {playerInfos.head.title}
          {playerInfos.head.sex === 'M' ? <Male /> : <Female />}
        </SC.Title>
      </SC.HeadDiv>
      <InfoTable data={playerInfos.infos} />
      <Bubble icon={<Trophy />}>
        <BubbleKeyDiv>Points won: </BubbleKeyDiv>
        <b>{playerInfos.head.points}</b>
      </Bubble>
    </SC.Wrapper>
  );
};

export default PlayerSheet;
