import styled from 'styled-components';

export const Wrapper = styled.div(({ theme }) => `
  background-color: ${theme.background.elem.primary};
  width: 800px;
  margin: auto;
  margin-top: 2vh;
  display: flex;
  flex-direction: column;
  padding: 1em;
`);

export const HeadDiv = styled.div`
  display: flex;
  flex-direction: row;
  align-items: center;
  justify-content: flex-start;
  gap: 10px;
  position: relative;
  min-height: 50px;
`;

export const Title = styled.h2(({ theme }) => `
  color: ${theme.color.secondary};
  font-weight: bold;
  margin: auto;
  display: flex;
  gap: 10px;
  align-items: center;
`);

export const ContentDiv = styled.div`
  display: flex;
  flex-flow: row;
`;

export const ProfileImg = styled.img`
  width: 150px;
  height: 150px;
  margin: auto;
  border-radius: 100px;
`;

export const FlagImg = styled.img`
  width: 50px;
  height: 30px;
  position: absolute;
  top: 10px;
  left: 0;
`;
