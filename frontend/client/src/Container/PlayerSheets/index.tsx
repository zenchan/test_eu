/* eslint-disable react/jsx-key */
import api from '@/api';
import { useEffect, useMemo, useState } from 'react';
import { Card } from '@/Components/Card';
import { InfoTable } from '@/Components/InfoTable';
import { Bubble } from '@/Components/Bubble';
import { Trophy } from '@/Components/Icons';
import { playerToSheet } from '@/common/playerToSheet';
import * as SC from './style';
import * as T from './types';

export const PlayerSheets = () => {
  const [players, setPlayer] = useState<T.Player[]>([]);
  const [error, setError] = useState<any>(null);

  const fetchPlayers = async () => {
    try {
      const response = await api.players();
      setPlayer(response?.data || []);
    } catch (err) {
      setError(err);
    }
  };

  useEffect(() => {
    fetchPlayers();
  }, []);

  const playersInfos: T.PlayersInfos = useMemo(() => {
    const values = players.map((player) => playerToSheet(player));
    return (values);
  }, [players, setPlayer]);

  if (!playersInfos) return <div>Loading...</div>;
  if (error) {
    return (<div>{`ERROR:${error.code}: ${error.message}`}</div>);
  }

  return (
    <SC.Wrapper>
      {playersInfos.map(({ head, infos }) => (
        <Card {...head}>
          <SC.TableTitle>
            stats
          </SC.TableTitle>
          <InfoTable
            data={infos}
          />
          <Bubble icon={<Trophy />}>
            <SC.BubbleKeyDiv>Points won: </SC.BubbleKeyDiv>
            <b>{head.points}</b>
          </Bubble>
        </Card>
      ))}
    </SC.Wrapper>
  );
};

export default PlayerSheets;
