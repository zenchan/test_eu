import styled from 'styled-components';

export const Wrapper = styled.div`
  display: flex;
  justify-content: space-around;
  padding: 2vh 2vw;
  flex-flow: row wrap;
  gap: 2em;
`;

export const TableTitle = styled.div(({ theme }) => `
  margin: 10px 0 0px 0px;
  color: ${theme.color.primary};
  font-weight: bold;
  text-transform: uppercase;
`);

export const BubbleKeyDiv = styled.div(({ theme }) => `
  color: ${theme.color.secondary};
`);
