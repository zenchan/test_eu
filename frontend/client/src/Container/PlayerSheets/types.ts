export interface Ping {
  ping: string;
}

export interface Country {
  picture: string;
  code: string;
}

export interface Data {
  rank: number;
  points: number;
  weight: number;
  height: number;
  age: number;
  last: number[];
}

export interface Player {
  id: number;
  firstname: string;
  lastname: string;
  shortname: string;
  sex: string;
  country: Country;
  picture: string;
  data: Data;
}

export interface Head {
  key: number;
  href: string;
  color: string;
  title: string;
  picture: string;
  subtitle: string;
  points: number;
  [key: string]: any;
}

export interface Info {
  key: string;
  value: unknown;
}

export interface PlayerInfo {
  head: Head,
  infos: Info[];
}

export type PlayersInfos = PlayerInfo[];
