import { render, screen } from '@testing-library/react';
import { ThemeProvider } from 'styled-components';
import theme from '../../../theme/default';
import { Card } from '..';

const CardProps = {
  children: (<div>test</div>),
  href: '#href',
  picture: '#picture',
  title: 'title',
  color: 'red',
  subtitle: 'subtitle',
};

test('Should render a children and icon', () => {
  render(
    <ThemeProvider theme={theme}>
      <Card {...CardProps} />
    </ThemeProvider>,
  );
  const title = screen.getByText('title');
  const subtitle = screen.getByText(/subtitle/i);
  const children = screen.getByText(/test/i);

  expect(screen.getByText('see details').href).toBe('http://localhost/#href');
  expect(title).toBeInTheDocument();
  expect(subtitle).toBeInTheDocument();
  expect(children).toBeInTheDocument();
});
