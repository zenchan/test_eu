import { ChevronRight } from '@/Components/Icons';
import * as T from './card.types';
import * as SC from './card.style';

export const Card: T.Card = ({
  children, href, onClick, picture, title, subtitle, color,
}) => (
  <SC.Wrapper onClick={onClick}>
    <SC.HeadDiv>
      <SC.ProfilePictureImg color={color} alt="profile picture" src={picture} />
      <SC.TitlesDiv>
        <SC.Title>{title}</SC.Title>
        <SC.SubTitle>{subtitle}</SC.SubTitle>
      </SC.TitlesDiv>
    </SC.HeadDiv>
    {children}
    <SC.LinkA href={href}>
      see details
      <ChevronRight />
    </SC.LinkA>
  </SC.Wrapper>
);

export default Card;
