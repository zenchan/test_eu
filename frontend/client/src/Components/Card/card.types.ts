import React, { FC } from 'react';

export interface CardProps {
  children?: React.ReactElement | React.ReactElement[];
  href: string;
  picture: string;
  title: string;
  color: string;
  subtitle: string;
  onClick?: () => void;
}

export type Card = FC<CardProps>;
