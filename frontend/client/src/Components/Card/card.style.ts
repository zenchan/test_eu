import Link from 'next/link';
import styled from 'styled-components';

export const Wrapper = styled.div(({ theme }) => `
  width: 35vh;
  min-width: 250px;
  background-color: ${theme.background.elem.primary};
  display: flex;
  flex-direction: column;
  box-shadow: ${theme.boxShadow.default};
  border-radius: 5px;
  cursor: default;
  padding: 10px;
  &:hover {
    box-shadow: ${theme.boxShadow.hover};
  }
`);

export const HeadDiv = styled.div`
  display: flex;
  flex-flow: column;
  gap: 10px;
  justify-content: center;
  align-items: center;
`;

export const ProfilePictureImg = styled.img(({ color }) => `
  border-radius: 100px;
  height: 100px;
  width: 100px;
  border: 2px solid ${color};
`);

export const TitlesDiv = styled.div`
  display: flex;
  flex-direction: column;
  gap: 5px;
  align-items: center;
  justify-content: center;
`;

export const Title = styled.h2(({ theme }) => `
  margin: 5px;
  color: ${theme.color.secondary};
`);

export const SubTitle = styled.h4(({ theme }) => `
  margin: 5px;
  color: ${theme.color.primary};
`);

export const LinkA = styled(Link)(({ theme }) => `
  color: ${theme.color.secondary};
  margin-left: auto;
  text-decoration: none;
  display: flex;
  flex-direction: row;
  align-items: center;
  font-size: 1em;
  justify-content: center;
  cursor: pointer;
  margin-top: 5px;
  gap: 5px;
  &:hover {
    color: ${theme.color.primary};
  }
`);
