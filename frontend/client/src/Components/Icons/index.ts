export { Female } from './female';
export { Male } from './male';
export { Trophy } from './trophy';
export { ChevronRight } from './chevron';
