import { render, screen } from '@testing-library/react';
import { ThemeProvider } from 'styled-components';
import theme from '../../../theme/default';
import { InfoTable } from '..';

const infoTableProps = [
  { key: 'test key', value: 'test value' },
  { key: 'test key 2', value: 'test value 2' },
];

test('Should render a children and icon', () => {
  render(
    <ThemeProvider theme={theme}>
      <InfoTable data={infoTableProps} />
    </ThemeProvider>,
  );
  const key1 = screen.getByText('test key:');
  const key2 = screen.getByText('test key 2:');
  const value1 = screen.getByText('test value');
  const value2 = screen.getByText('test value 2');

  expect(key1).toBeInTheDocument();
  expect(key2).toBeInTheDocument();
  expect(value1).toBeInTheDocument();
  expect(value2).toBeInTheDocument();
});
