import * as SC from './style';
import * as T from './types';

export const InfoTable: T.InfoTable = ({ data }) => (
  <SC.WrapperUl>
    {data.map(({ key, value }) => (
      <SC.LineLi key={key}>
        <SC.KeySpan>
          {key}
          :
          {' '}
        </SC.KeySpan>
        <SC.ValueSpan>{value}</SC.ValueSpan>
      </SC.LineLi>
    ))}
  </SC.WrapperUl>
);

export default InfoTable;
