import styled from 'styled-components';

export const WrapperUl = styled.ul(({ theme }) => `
  background-color: ${theme.background.color};
  list-style-type: none;
  padding: 0.5em;
  display: flex;
  flex-direction: column;
  gap: 5px;
  margin-top: 5px;
`);

export const Title = styled.h3`
  margin: 0;
  text-transform: uppercase;
`;

export const LineLi = styled.li(({ theme }) => `
  color: ${theme.color.secondary};
`);

export const KeySpan = styled.span`
`;

export const ValueSpan = styled.span`
  font-weight: bold;
`;
