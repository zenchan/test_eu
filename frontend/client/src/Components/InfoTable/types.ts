import { FC } from 'react';

export interface Data {
  key: string;
  value: any;
}

export interface InfoTableProps {
  data: Data[];
}

export type InfoTable = FC<InfoTableProps>;
