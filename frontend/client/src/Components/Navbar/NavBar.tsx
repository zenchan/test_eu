import * as T from './types';
import * as SC from './styles';

export const NavBar: T.NavBar = ({ children }) => (
  <SC.NavBarDiv>
    {children}
  </SC.NavBarDiv>
);

export default NavBar;
