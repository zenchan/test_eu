import React, { FC } from 'react';

export interface NavbarProps {
  children: React.ReactElement;
}

export type NavBar = FC<NavbarProps>;
