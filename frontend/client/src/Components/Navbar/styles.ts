import styled from 'styled-components';

export const NavBarDiv = styled.div(({ theme }) => `
  width: 100%;
  background-color: ${theme.background.elem.secondary};
  color: ${theme.color.light};
  display: flex;
  flex-direction: row;
  justify-content: center;
  align-items: center;
  font-size: ${theme.fontSize.xl};
  padding: ${theme.padding.default} 0;
`);

export default NavBarDiv;
