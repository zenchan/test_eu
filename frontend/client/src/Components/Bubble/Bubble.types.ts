import React, { FC } from 'react';

export interface BubbleProps {
  children: React.ReactElement | React.ReactElement[];
  icon: React.ReactElement;
}

export type Bubble = FC<BubbleProps>;
