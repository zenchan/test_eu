import styled from 'styled-components';

export const Wrapper = styled.div(({ theme }) => `
  background-color: ${theme.background.elem.secondary};
  color: ${theme.color.light};
  display: flex;
  flex-flow: row;
  justify-content: center;
  align-items: center;
  padding: 10px;
  gap: 5px;
`);

export const IconDiv = styled.div`
`;
