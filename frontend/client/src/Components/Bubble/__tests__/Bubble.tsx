import { render, screen } from '@testing-library/react';
import { ThemeProvider } from 'styled-components';
import theme from '../../../theme/default';
import { Bubble } from '..';

test('Should render a children and icon', () => {
  render(
    <ThemeProvider theme={theme}>
      <Bubble icon={<div>TT</div>}>
        <div>TEST</div>
      </Bubble>
    </ThemeProvider>,
  );
  const icon = screen.getByText(/TT/i);
  const children = screen.getByText(/TEST/i);

  expect(icon).toBeInTheDocument();
  expect(children).toBeInTheDocument();
});
