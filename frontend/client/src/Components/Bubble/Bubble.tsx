import * as SC from './Bubble.style';
import * as T from './Bubble.types';

export const Bubble: T.Bubble = ({ children, icon }) => (
  <SC.Wrapper>
    {icon}
    {children}
  </SC.Wrapper>
);

export default Bubble;
