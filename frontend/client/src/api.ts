/* eslint-disable no-param-reassign */
import { API_URL } from '@/env';
import axios from 'axios';

export const instance = axios.create({
  baseURL: API_URL,
  timeout: 1000,
});

instance.interceptors.request.use((config) => {
  config.headers['Access-Control-Allow-Origin'] = API_URL;
  config.headers['Access-Control-Allow-Methods'] = 'GET, POST, PUT, DELETE';
  config.headers['Access-Control-Allow-Headers'] = 'Content-Type';
  return config;
}, (error) => Promise.reject(error));

export const api = {
  players: () => instance.get('/players'),
  player: (id: string) => instance.get(`/players/${id}`),
};

export default api;
