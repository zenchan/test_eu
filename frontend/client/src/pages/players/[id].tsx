import { PlayerSheet } from '@/Container/PlayerSheet';

export default function Home() {
  return (
    <main>
      <PlayerSheet />
    </main>
  );
}
