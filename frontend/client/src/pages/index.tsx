import { PlayerSheets } from '@/Container/PlayerSheets';

export default function Home() {
  return (
    <main>
      <PlayerSheets />
    </main>
  );
}
