export const PORT = process.env.PORT || '3001';
export const API_URL = process.env.NODE_ENV === 'production' ? process.env.API_URL : 'http://localhost:3001';
