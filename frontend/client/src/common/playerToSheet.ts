import { PlayerToSheet } from './playerToSheet.types';

export const playerToSheet: PlayerToSheet = (player) => {
  const head = {
    key: player.id,
    href: `/players/${player.id}`,
    color: player.sex === 'M' ? 'var(--color-blue)' : 'var(--color-pink)',
    title: `${player.firstname} ${player.lastname}`,
    flag: player.country.picture,
    picture: player.picture,
    subtitle: `Rank: ${player.data.rank}`,
    points: player.data.points,
    sex: player.sex,
  };
  const infos = [
    { key: 'age', value: player.data.age },
    { key: 'sex', value: player.sex },
    { key: 'country', value: player.country.code },
    { key: 'weight', value: player.data.weight },
    { key: 'height', value: player.data.height },
  ];
  return ({ head, infos });
};

export default playerToSheet;
