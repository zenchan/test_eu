import { Player, PlayerInfo } from '@/Container/PlayerSheets/types';

// eslint-disable-next-line no-unused-vars
export type PlayerToSheet = (p: Player) => PlayerInfo;
